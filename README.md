## Instrucciones agregar Indigo como una dependencia o submodulo

1. En tu repositorio, puedes consumir los estilos de Indigo agregándolos como una dependencia o submódulo.
cmd: `git submodule add <URL_del_repositorio_indigo> lib/indigo` Esto creará un submódulo en la carpeta lib/estilolib de tu repositorio
2. Importa los estilos en tu proyecto principal. Supongamos que tienes un archivo main.scss: `@import 'lib/indigo/style/variables';`
3. Si se realizan cambios en Indigo, puedes actualizar tu repositorio para incluir esos cambios: `git submodule update --remote --merge`

¡Happy design :)!

---------------------------------------------------------------


Este proyecto utiliza Sass para la generación de estilos CSS.

## Estructura de Carpetas

- `src/`: Contiene los archivos fuente.
  - `scss/`: Archivos Sass.
- `public/`: Contiene los archivos compilados.
  - `styles/`: Archivos CSS.

## Instrucciones de Ejecución

1. Clona el repositorio: `git clone <url-del-repositorio>`
2. Instala las dependencias: `npm install`
3. Ejecuta el script de Sass: `npm run sass`

## Configuración
Inicializar el proyecto de npm:
npm init --yes

Instalar node-sass:
npm install node-sass --save-dev
Estructura de Carpetas y Archivos:
my-sass-project/
├── src/
|   ├── scss/
|   |   └── main.scss  # Archivo SCSS principal
├── public/
|   └── styles/
|   |   └── main.css   # Archivo CSS compilado
|   └──  index.html     # Archivo HTML de ejemplo
└── package.json        # Archivo de configuración de npm


Script de Compilación en package.json:
Agrega el siguiente script al archivo package.json:

 "scripts": {
    "sass": "node-sass src/scss/main.scss -o public/styles --watch"
  },

Cómo Ejecutar:
npm run sass
Esto iniciará la compilación de Sass en modo de observación.

Gitlab page plain html - archivo .gitlab-ci.yml
image: alpine:latest

pages:
  stage: deploy
  script:
    - echo "happy desing :)"
  artifacts:
    paths:
      - public
  only:
  - main 
